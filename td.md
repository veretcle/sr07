Fichier distribué sous licence Creative Commons [CC-BY-SA](http://creativecommons.org/licenses/by-sa/2.0/)

# Objectif général du projet

L’objectif général du projet est de mettre en place NextCloud, une application Web de stockage et de partage de données, dans un mode le plus redondant possible. Pour cela, nous allons construire :
* un stockage redondant qui assurera la fonction de NAS (via le protocole NFS)
* deux serveurs MariaDB en cluster, assurant redondance des données et de leur accès
* deux serveurs d’exécution PHP-FPM, pour la partie applicative
* deux serveurs de présentation NginX, pour la partie présentation, avec une adresse virtuelle VRRP pour la redondance frontale

Voici la vue sous forme de service de l’ensemble de cette architecture :

```mermaid
flowchart TD
    subgraph présentation
    direction LR
    nginx1-- vrrp ---nginx2
    end
    subgraph exécution
    direction LR
    php1----php2
    linkStyle 1 stroke-width:0px;
    end
    subgraph BDD
    direction LR
    mariadb1-- sync ---mariadb2
    end
    subgraph stockage
    store1
    end
    subgraph annuaire
    ldap1
    end
    présentation-- NAS -->stockage
    exécution-- NAS -->stockage
    exécution-- authentifie -->annuaire
    présentation-- appelle -->exécution
    exécution-- appelle -->BDD
```

Et la vue sous forme de machines :

```mermaid
graph TD
subgraph serveur applicatif 1
nginx1
php1
mariadb1
end
subgraph serveur applicatif 2
nginx2
php2
mariadb2
end
subgraph serveur annuaire
ldap1
end
subgraph serveur stockage
store1
end
```

> On ne s’inquiète pas, ça va bien se passer :)

# Prélude

Vous avez à votre disposition une VM template [Cloud-Image](https://cloud.debian.org/images/cloud/bookworm/latest/). Cette image n’a pas grand-chose à voir avec le cloud, elle permet juste de disposer d’un système prêt à installer sous forme de machine virtuelle et inclut le logiciel [Cloud-Init](https://cloudinit.readthedocs.io/en/latest/index.html) qui permet l’initialisation simplifiée.

## Cloner une machine

Lorsque vous clonez ce template, vous devez modifier les paramètres suivants :
* dans la section `Hardware`, agrandissez le disque de 18G
* dans la section `Cloud-Init` :
    * initialiser l’utilisateur (non root), le mot de passe et poser une clé (publique) SSH
    * régler l’IP Config en DHCP + SLAAC

Vous pouvez alors démarrer la machine, elle devrait automatiquement avoir tous les bons paramètres.

# Construction d’un serveur NAS avec stockage redondant

## Installation du système

Clonez une première machine. Cette machine servira de stockage partagé pour l’ensemble des autres machines. Associez 2 disques de taille égale (environ 20 Gio).

> Pour cette première machine, je vous conseille de faire un clone complet. Les suivantes pourront utiliser des clones liés si nécessaires.

## Systèmes redondants de données: RAID

Objectif de cette partie :

```mermaid
graph LR
subgraph Physical Block Device
/dev/vda
/dev/vdb
end
subgraph Virtual Block Device
/dev/md0 --> /dev/vda
/dev/md0 --> /dev/vdb
end
```

> Note: installez `mdadm` pour cette partie

### Création d’un RAID1 logiciel

> Vous pouvez vérifier les partitions/disques à votre disposition avec la commande `lsblk`

Vous allez créer un array en RAID1 à partir de deux disques virtuelles. À partir des deux block devices physiques disponibles (`/dev/vda` et `/dev/vdb`), vous allez pouvoir construire un block device virtuel grâce à la commande `mdadm` (`/dev/md0` dans notre cas) :

```
# mdadm --create /dev/md0 --level=1 --raid-devices=2 /dev/vda /dev/vdb
```

`--level` permet de spécifier le type de RAID que vous souhaitez (0, 1, 5, 10, etc…). `--raid-devices` précise le nombre de partitions ou block devices physiques concernés par ce RAID. L’ordre dans lequel vous listez les partitions va déterminer quelle partition sera considérée comme maître.

Vous pouvez constater la progression de la construction (ou de la reconstruction) du RAID via le fichier `/proc/mdstat`.

```
# cat /proc/mdstat
Personalities : [raid1]
md0 : active raid1 vdb[1] vda[0]
      38981632 blocks [2/2] [UU]
unused devices: <none>
```

`[0]` indique le volume maître. `[2/2]` indique le nombre de disques online pour cet array et `[UU]` les disques online.

> Que se passe-t-il maintenant quand le système veut écrire sur le block device virtuel `/dev/md0` ?

### Mise en défaut du RAID

Pour l’instant, votre RAID1 se porte bien. Voyons ce qui se produit si nous le titillons un peu…

Commencez par formater le block device `/dev/md0` que vous avez créé en `ext4` :
```
# mkfs.ext4 /dev/md0
```

Remarquez au passage qu’il est vu comme une simple block device. Montez la partition dans `/mnt` :

```
# mount /dev/md0 /mnt
```

Placez quelques fichiers dedans (copiez des fichiers de `/etc` par exemple).

Vérifiez l’état de votre RAID :

```
# mdadm --detail /dev/md0
```

Maintenant, nous allons artificiellement déclarer un des block devices composant le RAID comme défectueux :

```
# mdadm --manage /dev/md0 --fail /dev/vdb
mdadm: set /dev/vdb faulty in /dev/md0
```

Vous pouvez maintenant constater, dans le fichier `/proc/mdstat` que votre RAID fonctionne en mode dégradé.

> Pouvez-vous toujours accéder à vos données dans `mnt` ? Vos fichiers ont-ils été endommagés ? Pouvez-vous toujours écrire ou modifier des fichiers dans `/mnt` ?

Enlevez le volume défaillant du groupe RAID :

```
# mdadm --manage /dev/md0 --remove /dev/vdb
mdadm: hot removed /dev/vdb
```

Jetez un œil dans /proc/mdstat pour constater que le volume est à présent marqué comme manquant. Vous pouvez de nouveau l’ajouter au groupe RAID et constater que ce dernier va se reconstruire tout seul :

```
# mdadm --manage /dev/md0 --add /dev/vdb
mdadm: re-added /dev/vdb
```

Vérifiez que le disque est bien en cours de reconstruction :
```
# cat /proc/mdstat
Personalities : [raid1]
md0 : active raid1 vdb[2] vdc[1]
      10476544 blocks super 1.2 [2/1] [_U]
      [=>...................]  recovery =  9.5% (1001088/10476544) finish=0.7min speed=200217K/sec

unused devices: <none>
```

Tant qu’il est en reconstruction, il devrait l’indiquer dans son état :
```
# mdadm --detail /dev/md0 | grep State
             State : clean, degraded, recovering
```

Évidemment, pour l’utilisateur, la défaillance du disque, sa suppression du groupe RAID et l’ajout d’un « nouveau » disque ont été complètement transparents.

> Que se passe-t-il si le maître est marqué comme défaillant au moment de la reconstruction du RAID ?

## Gestionnaire de volumes logiques : LVM

```mermaid
graph LR
subgraph Logical Volume
lv1
end
subgraph Volume Group
vgsr07
end
subgraph Virtual Block Device
/dev/md0
end
subgraph Physical Block Device
/dev/vda
/dev/vdb
end
/dev/md0 --> /dev/vda
/dev/md0 --> /dev/vdb
vgsr07 --> /dev/md0
lv1 --> vgsr07
```

> Note : `lv1` va du coup être aussi un block device virtuel
> Note : pour cette partie, installez le paquet `lvm2`

### Création d’un Volume Group

Démontez `/dev/md0` de votre arborescence :

```
# umount /mnt
```

Créez un Physical Volume sur le block device virtuel du RAID :

```
# pvcreate /dev/md0
```

Le volume physique est à présent initialisé correctement. Créez un groupe de volumes :

```
# vgcreate vgsr07 /dev/md0
```

Le VG est à présent prêt pour recevoir des volumes logiques. Contrôlez le nombre de physical extents disponibles dans votre VG :

```
# vgdisplay
  --- Volume group ---
  VG Name               vgsr07
  System ID
  Format                lvm2
  [...]
  VG Size               672,00MB
  PE Size               32,00 MB
  Total PE              21
  Alloc PE / Size       0 / 0,00 MB
  Free  PE / Size       21 / 672,00 MB
  VG UUID               Dngwte-RuVl-CdqX-fEAG-2YE9-BdiS-EE12x6
```

Créez en conséquence un LV :

```
# lvcreate --size 1G --name lvtest vgsr07
```

Créez un système de fichiers dans ce LV (`mkfs.ext4 /dev/vgsr07/lvtest`) et montez-le dans votre arborescence (dans `/mnt` par exemple).

> Quel est le type du fichier /dev/<volume_group>/<logical_volume> ?

### Agrandissement du Logical Volume

Pour illustrer l’une des grandes forces de LVM, nous allons agrandir, à chaud, le volume logique vous avez créé.

Étendez le LV 

```
# lvextend --size +1G /dev/vgsr07/lvtest
```

À présent, redimensionnez votre système de fichiers pour le faire correspondre à la taille du LV :

```
# resize2fs /dev/vgsr07/lvtest
```

> Que constatez-vous au niveau du volume logique monté ? Y a-t-il eu interruption de service ? Est-ce qu’une opération de ce type est possible avec des partitions standards ?
> Est-ce que la même opération est possible pour réduire un volume logique ? Quelles précautions faut-il prendre ?

### Manipulation de snapshots

Placez quelques fichiers sur le volume logique monté que vous venez d’agrandir. Vérifiez qu’il vous reste de la place dans le groupe de volume correspondant. Créez un volume de snapshot à partir du volume montée :

```
# lvcreate --snapshot --extents 200 --name lvtest_snap /dev/vgsr07/lvtest
```

Regardez le statut des volumes logiques :

```
# lvs
  LV        VG   Attr   LSize  Origin Snap%  Move Log Copy%  Convert
  lvtest    test owi-ao  1,83G
  test_snap test swi-a- 40,00M lvtest   0,00
```

Supprimez des fichiers du volume logique monté. Vérifiez de nouveau le statut des volumes logiques. Essayez de monter en lecture seule le volume de snapshot.

> Que constatez-vous quant à la taille du snapshot une fois les fichiers supprimés ? Quel est le contenu du snapshot que vous avez monté ?

Essayez de faire un autre snapshot et faites quelques manipulations de fichiers supplémentaires.

> Que pensez-vous des performances disque d’un tel système ? Est-il possible de l’utiliser dans des conditions de production ? Sous quelles réserves ?

Montez une des partitions de snapshot en écriture (option rw sur mount). Modifiez un fichier.

> Est-ce que le fichier d’origine a été modifié ? Que pouvez-vous envisager de faire grâce à ce genre de manipulation ?

## Partage NAS avec NFS

Avant de procéder, nous allons détruire les logical volumes associés à lvtest :

```
# lvremove /dev/vgsr07/lvtest_snap
# lvremove /dev/vgsr07/lvtest
```

### Configuration du serveur NFS

Ensuite, installez les paquets nécessaires pour transformer votre machine en serveur NFS :

```
# apt install nfs-kernel-server
```

Créez un logical volume (5 Gio devrait suffire), formatez-le en `ext4`, montez-le dans `/srv` et ajoutez-le dans `/etc/fstab`.

La syntaxe pour `/etc/fstab` est la suivante :
```
<block device> <point de montage> <options séparées par des virgules> 0 0
```

Une fois que cela est fait, il faut recharger `systemctl`. Vous pouvez également vérifier que vous n’avez pas fait d’erreur de syntaxe en utilisant la commande `mount` pour demander le montage de tous les systèmes de fichiers :

```
# systemctl daemon-reload
# mount -a
```

Éditez le fichier `/etc/exports` (exemple ci-dessous, à adapter) :

```
/srv	10.0.2.0/24(rw,sync,no_subtree_check)
```

> La ligne de configuration se lit comme suit : système de fichier local, adresses vers lesquelles il est exporté puis droit. Vous devez bien évidemment adapter cette configuration.

Redémarrez le service `nfs-server` pour rendre la configuration effective.

### Configuration d’un client NFS

Clonez de nouveau votre machine de base et démarrez-la. Nous allons nous en servir comme client NFS et vérifier que tout fonctionne correctement. Installez les paquets NFS client :

```
# apt install nfs-common
```

Vous pouvez ensuite constater que le partage est effectif :

```
# showmount -e <ip du serveur>
```

Montez-le dans le répertoire `/mnt` :

```
mount -t nfs <ip du serveur>:/srv /mnt
```

> Pouvez-vous écrire des fichiers sur ce montage ? Pourquoi ?

Nous allons créer un dossier `test` du côté du serveur NFS qui sera accessible à l’utilisateur ayant l’UID 1000 (le premier utilisateur du système dans la configuration actuelle).

Sur le serveur, créez un répertoire dans `/srv` (le point de montage du serveur NFS) et donnez-lui les bonnes autorisations.

> Peut-on alors écrire des données dans l’utilisateur 1000 depuis le client ? Que constatez-vous côté serveur NFS ?

Nous allons tester la persistence du montage. Éditez le fichier `/etc/fstab` sur le client et ajoutez la ligne suivante :

```
<ip du serveur>:/srv	/srv	nfs	users,noauto,x-systemd.automount,x-systemd.device-timeout=10	0	0
```

Redémarrez et vérifiez que le client NFS se déclenche lorsque vous essayez d’accéder au dossier `/srv`.

> Vous pouvez créer un dossier `/srv/http` et lui attribuer les droits de l’UID 33 (utilisateur `www-data` sur tous les systèmes que nous allons installer).

Si tout fonctionne correctement, vous pouvez éteindre cette machine, elle servira plus tard de machine d’exécution/présentation NginX/PHP.

# Serveur LDAP

## LDAP

Clonez une nouvelle machine.

Installez OpenLDAP :

```
# apt install slapd ldap-utils
```

Après l’installation, utilisez l’utilitaire de configuration de paquet pour créer la racine de votre annuaire LDAP ainsi que le compte administrateur de l’annuaire :

```
dpkg-reconfigure slapd
```

L’utilitaire vous guidera dans la création de votre annuaire. Donnez comme nom DNS :

```
sr07.utc
```

Donnez à votre racine un nom de type :

```
dc=sr07,dc=utc
```

> Attention ! Les deux doivent avoir des noms similaires…

### Création des Organizational Units

Notre arbre va avoir la représentation suivante :

```mermaid
graph TD
item1["dc=utc"] --> item2["dc=sr07"]
item2 --> item3["ou=Users"]
item2 --> item4["ou=Groups"]
```

Commencez par créer un fichier LDIF et ajoutez-y les informations suivantes :

```
dn: ou=Users,dc=sr07,dc=utc
objectclass: organizationalUnit
ou: Users
```

Vous venez de définir une Organizational Unit ou OU. Ajoutez ces informations au LDAP avec la commande `ldapadd` :

```
$ ldapadd -x -D 'cn=admin,dc=sr07,dc=utc' -W -f init.ldif
```

> -x précise que l’on utilise pas de connexion sécurisée (en temps normal, il faudrait utiliser une connexion chiffrée mais ce n’est pas le but de l’exercice) et il faudra donc le mettre systématiquement. -D permet de se *binder* au LDAP en tant qu’utilisateur `admin`.

Vous devriez normalement pouvoir voir le contenu du LDAP :

```
$ ldapsearch -x -b 'dc=sr07,dc=utc' '(objectclass=*)'
```

Sur le même principe, vous allez créer l’OU Groups qui contiendra les groupes d’utilisateurs, sur le même modèle que précédemment.

### Création d’un carnet minimal d’utilisateurs

Vous pouvez créer des utilisateurs dans le LDAP avec la commande ldapadd(1) et quelques fichiers LDIF. Voici un exemple d’utilisateur et de groupe :

```
dn: uid=cveret,ou=Users,dc=sr07,dc=utc
uid: cveret
cn: Clément VÉRET
objectClass: account
objectClass: posixAccount
objectClass: top
objectClass: shadowAccount
userPassword: {SSHA}<hash de mot de passe>
loginShell: /bin/bash
uidNumber: 600
gidNumber: 10000
homeDirectory: /home/cveret
```

> Vous pouvez facilement obtenir un hash de mot de passe à l’aide de la commande `slappasswd`.

Créez le groupe primaire de cet utilisateur, ainsi qu’un groupe secondaire :

```
dn: cn=groupe_td,ou=Groups,dc=sr07,dc=utc
objectClass: posixGroup
objectClass: top
cn: groupe_td
gidNumber: 10000

dn: cn=groupe_secondaire,ou=Groups,dc=sr07,dc=utc
objectClass: posixGroup
objectClass: top
cn: groupe_secondaire
gidNumber: 10001
memberUid: cveret
```

Après les avoir ajoutés, vérifiez à l’aide de `ldapsearch` que les utilisateurs et groupes sont correctement définis. Créez quelques utilisateurs supplémentaires sur le même modèle.

Vérifiez également que les mots de passe sont correctement définis : utilisez l’un des utilisateurs créés au lieu de l’utilisateur admin pour vous binder au LDAP lors de vos recherches.

# Machines applicatives

Rallumez votre client NFS de tout-à-l’heure et clonez une machine supplémentaire. Sur cette seconde machine, montez le même partage NFS que précédemment dans `/srv`.

> Il est rappelé que chaque composant du TD pourrait être mis sur une machine physique, une machine virtuelle, un conteneur léger type LXC ou un conteur Docker. Tout dépend de l’usage et du contexte dans lequel vous avez à faire tourner ce type d’infrastructure.

**À partir de ce moment, toutes les manipulations seront à faire sur les deux serveurs applicatifs que vous venez de cloner.**

# Cluster MariaDB
## Installation de MariaDB et paramétrage de Galera Cluster

Installez MariaDB sur chacune des machines :

```
# apt update && apt install mariadb-server
```

Créez un fichier `/etc/mysql/mariadb.conf.d/galera.cnf` avec le contenu suivant sur le nœud 1 du cluster :

```
[mysqld]
binlog_format=ROW
wsrep_provider=/usr/lib/libgalera_smm.so
wsrep_cluster_name="cluster_sr06"
wsrep_cluster_address="gcomm://"
wsrep_sst_method=rsync
wsrep_on=ON
wsrep_node_address="<adresse IP nœud 1>"
wsrep_node_name="node1"
```

Cette configuration va permettre d’initialiser le nœud MariaDB du cluster Galera. Arrêtez le service `mariadb` et lancez la commande suivante :

```
# galera_new_cluster
```

Relancez le service `mariadb`. Le premier nœud est initialisé. Sur le second nœud, créez également un fichier `/etc/mysql/mariadb.conf.d/galera.cnf` similaire au premier. Changez simplement l’adresse du nœud et indiquez cette fois-ci l’adresse du cluster Galera :

```
wsrep_cluster_address="gcomm://<adresse IP nœud 1>,<adresse IP nœud 2>"
```

Redémarrez le service `mariadb` sur le second nœud. Revenez sur le premier nœud et indiquez de nouveau l’adresse complète du cluster (comme ci-dessus).

À partir de maintenant, **les deux nœuds sont synchrones**. Il faut donc impérativement que l’un soit démarré pour que l’autre puisse démarrer (pour récupérer le log binaire des dernières transactions). Si vous devez redémarrer le cluster au complet, il faut impérativement retoucher la commande `wsrep_cluster_address` sur le premier nœud à démarrer avant de démarrer le second, puis remettre la valeur d’origine.

Si jamais le premier nœud refuse de redémarrer, il faut l’y forcer. Éditez la dernière ligne du fichier `/var/lib/mysql/grastate.dat` comme ceci :

```
safe_to_bootstrap: 1
```

Redémarrez MariaDB sur le premier nœud, puis sur le second et vérifiez bien que la configuration est symétrique entre les deux nœuds.

## Initialisation de la base de données MariaDB

Sur le nœud 1, lancez le script d’initialisation de la base et suivez les instructions :

```
# mysql_secure_installation
```

Connectez-vous au premier nœud du cluster et créez une base `nextcloud` et un utilisateur associé :

```
# mysql -u root
MariaDB [(none)]> CREATE DATABASE nextcloud;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO ncuser IDENTIFIED BY 'super_mot_de_passe';
```

Vérifiez sur le second nœud que vous pouvez bien vous connecter en tant qu’utilisateur `ncuser` et que vous voyez la base de données :

```
# mysql -u ncuser -psuper_mot_de_passe
MariaDB [(none)]> SHOW DATABASES;
```

Si c’est le cas, votre cluster MariaDB est bien synchrone.

> Lors de la configuration de votre instance NextCloud, vous ne pourrez préciser qu’une seule adresse IP pour le cluster MariaDB. Il est possible de prendre en compte les deux, mais cela exige une configuration plus avancée qui sort du cadre de ce TD.

Pour terminer la configuration, assurez-vous que MariaDB écoute bien sur toutes les interfaces réseaux (et toutes les adresses) en commentant la ligne ci-dessous dans le fichier `/etc/mysql/mariadb.conf.d/50-server.cnf` :

```
bind-address = 127.0.0.1
```

# NginX, PHP-FPM et VRRP
## Installation et paramétrage de Keepalived

> Utilisez l’interface ens18 branchée sur le réseau externe pour cette partie ! L’adresse fixe que vous utilisez sera sous la forme : 172.23.254.<numéro_de_groupe>

Keepalived permet de faire du VRRP (*Virtual Router Redundancy Protocol*) : une adresse IPv4 ou IPv6 virtuelle est présente sur la machine principale. Si cette dernière n’est plus joignable ou défaillante, l’adresse sera reprise automatiquement par une autre machine, garantissant ainsi la continuité de service.

Pour simplifier, nous n’allons faire de redondance que sur une adresse IPv4, mais c’est applicable de la même manière à un groupe d’adresses hétérogènes.

Installez Keepalived :

```
# apt install keepalived
```

### Paramétrage du maître VRRP

Nous allons maintenant créer une adresse IP virtuelle sur l’un des deux serveurs.

Les adresses IPv4 ou IPv6 virtuelles dans VRRP sont systématiquement associées à une interface réseau et à un numéro (le VRID, Virtual Router ID). Chaque machine faisant du VRRP fait une annonce en broadcast sur son segment réseau à intervalle régulier pour signaler qu’elle est vivante et est capable de prendre en charge un VRID (et donc une ou plusieurs adresses IP associées). Cela permet d’avoir plusieurs communautés VRRP dans le même segment réseau.

Chaque VRID est ensuite regroupé dans un groupe VRRP. En cas de défaillance d’une interface dans le groupe, l’ensemble des adresses IP dans le VRID associé bascule sur une autre machine.

**Comme toutes vos machines virtuelles sont dans le même segment réseau, prenez votre numéro de groupe comme *VRID* et choisissez une adresse reprenant aussi votre numéro de groupe.**

On pourra par exemple prendre `172.23.254.<numéro de groupe>`

Pour notre cas, nous n’allons avoir qu’un seul groupe avec un seul VRID contenant une seule adresse pour une seule interface. Créez le fichier `/etc/keepalived/keepalived.conf` sur la machine maître avec le contenu suivant :

```
vrrp_sync_group VRRP_GRP1 {
	group {
		VIP_ETH
	}
}

vrrp_instance VIP_ETH {
	# State = Master ou Backup
	state MASTER
	# l’interface qui servira pour les annonces VRRP
	interface ens18
	# VRID
	virtual_router_id 10
	# la priorité de ce nœud VRRP dans le groupement
	priority 100
	version 3
	virtual_ipaddress {
		172.23.254.10
	}
}
```

> Choisissez bien soigneusement l’adresse de redondance de sorte à ce qu’elle ne puisse pas être en conflit avec d’autres adresses (choisissez-la hors de la plage DHCP de préférence)

La configuration du serveur secondaire se fait exactement de la même manière à deux détails près : `state` doit être réglé sur `BACKUP` et `priority` doit être amené à un nombre inférieur.

Une fois cette configuration faite, redémarrez le service `keepalived` vérifiez que vous pouvez joindre cette adresse depuis la machine hôte (avec `ping` par exemple). Vous pouvez également observer l’adresse secondaire sur l’interface configurée :

```
$ ip -4 address show dev ens18
2: ens18: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
	inet 172.23.6.78/24 brd 172.23.255.255 scope global eth0
		valid_lft forever preferred_lft forever
	inet 172.23.254.10/32 scope global secondary eth0
		valid_lft forever preferred_lft forever
```

> Que se passe-t-il si vous éteignez la machine maître ? Combien de temps faut-il pour que la bascule soit effective ?

## Installation et paramétrage de NginX

NextCloud ne peut plus fonctionner sans HTTPS. Nous allons donc créer un certificat auto-signé pour permettre à votre configuration de NginX de fonctionner correctement.

```
$ openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout nextcloud.key -out nextcloud.crt -days 365
```

Installez `nginx` et récupérez un fichier de configuration (à adapter) dans la [documentation officiel de NextCloud](https://docs.nextcloud.com/server/stable/admin_manual/installation/nginx.html).

Il sera nécessaire d’y faire les adaptations suivantes :
* chemin des certificats SSL (mot-clé `ssl_certificate` et `ssl_certificate_key`)
* URI du site (mot-clé `server_name`) que vous pouvez initialiser à une valeur quelconque (`nextcloud.utc` par exemple)
* adresse IP des deux serveurs applicatifs pour la partie PHP (section `upstream`)
* chemin d’accès à NextCloud (dans votre partage NAS en `/srv/http/nextcloud` par exemple, mot-clé `root`)
* une petite erreur s’est glissée dans la configuration de la documentation : à la ligne 89, il y a un `js` en trop pour l’interprétation des fichiers de _mimetype_ Javascript
* concaténer le fichier de configuration par défaut avec celui-ci.

> C’est normalement une **TRÈS TRÈS** mauvaise pratique, mais pour les besoins du TD, nous passerons outre.

À ce stade et même sans avoir installé NextCloud, vous devriez être en capacité d’accéder à votre site Web (qui fera une magnifique erreur 502…). Reproduisez la même configuration sur le second serveur applicatif.

## Installation et paramétrage de PHP-FPM

**⚠️ si vous avez utilisé un template Debian 11, la version de PHP va être trop ancienne pour Nextcloud.**
Il faut impérativement en installer une plus récente. Vous avez deux possibilités :
* mettre à jour votre Debian 11 vers Debian 12
* installer PHP8.2

Pour installer PHP 8.2 :
```
# apt update; apt -y install ca-certificates apt-transport-https software-properties-common wget gnupg2
# echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list
# wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add -
# apt update; apt -y install php8.2-fpm
```

**⚠️ vous pouvez maintenant reprendre le cours normal du TD**

Installez `php-fpm` et quelques unes des dépendances PHP nécessaires pour NextCloud :

```
apt install php8.2-fpm php8.2-opcache php8.2-mysql php8.2-mbstring php8.2-gd php8.2-intl php8.2-curl php8.2-zip php8.2-xml
```

Nous allons maintenant paramétrer PHP-FPM pour écouter sur un port plutôt que sur une socket Unix (son comportement par défaut). Éditez le fichier `/etc/php/8.1/fpm/pool.d/www.conf`, cherchez-y le mot-clé `listen` et remplacer le contenu d’origine par :

```
listen = 0.0.0.0:9000
```

Redémarrez le service `php-fpm` :

```
systemctl restart php8.2-fpm
```

Et normalement, PHP-FPM devrait maintenant écouter sur le port 9000.

[Téléchargez la dernière version de NextCloud Server](https://nextcloud.com/install/#instructions-server), décompressez-la dans le répertoire NAS monté sur l’une des deux machines. Paramétrez correctement les droits du répertoire (accessible en lecture/écriture pour l’utilisateur ayant l’UID/GID `33`). Assurez-vous qu’il est bien accessible par le même chemin sur les deux machines.

> Étant donné la façon dont fonctionne les droits d’accès, je vous recommande de le faire sur le NAS directement et d’y positionner les droits d’accès. Vous aurez besoin du logiciel `unzip` pour décompresser l’archive.

# Vos objectifs pour le projet

Pour le projet :
* vous devez paramétrer votre instance NextCloud pour que sa base de données soit stockée sur votre instance redondée MariaDB (vous pouvez laisser localhost)
* vous devez paramétrer le module LDAP de votre instance NextCloud pour aller chercher vos utilisateurs et vos groupes sur l’annuaire LDAP que vous avez paramétré ([encore une fois, la doc est là pour vous aider](https://docs.nextcloud.com/server/14/admin_manual/configuration_user/user_auth_ldap.html))
* lors du paramétrage LDAP, vous devrez préciser les classes d’objets correspondants à vos utilisateurs et groupes dans votre annuaire. Pour notre cas, la requête LDAP sera la suivante pour les utilisateurs :

```
(|(objetClass=posixAccount))
```
Et la suivante pour les groupes :

```
(|(objectClass=posixGroup))
```
* afin de montrer que tout cela fonctionne, vous devrez créer 3 utilisateurs dans NextCloud (userA, userB, userC), puis créer un partage de l’utilisateur userA vers les utilisateurs userB et userC. Lorsqu’un des utilisateurs se connectent, il doit être en capacité de voir le partage en question.
